﻿using ApplicationCore.DTOs.Tasks;
using ApplicationCore.DTOs.Users;
using ApplicationCore.Interfaces;
using ApplicationCore.Parameters;
using ApplicationCore.Wrappers;
using Microsoft.AspNetCore.Mvc;

namespace Host.Controllers
{
    [Route("api/users")]
    [ApiController]
    public class UserController: ControllerBase
    {
        private readonly IUserService _service;
        public UserController(IUserService service)
        {
            _service = service;
        }

        [HttpGet]
        public async Task<IActionResult> GetUsers([FromQuery] PaginationParameters parameters)
        {
            var pageNumber = parameters.PageNumber;
            var pageSize = parameters.PageSize;
            try
            {
                if(pageNumber == null && pageSize == null)
                {
                    var result = await _service.GetAllData();
                    return Ok(result);
                } else
                {
                    var result = await _service.GetPagedData((int)pageNumber, (int)pageSize);
                    return Ok(result);
                }
            } catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex);
            }

        }

        [HttpPost]
        public async Task<ActionResult<Response<int>>> CreateUsuario([FromBody] UsersDTO request)
        {
            var result = await _service.Create(request);
            if (result.Succeeded)
            {
                return Ok(result);
            }
            else
            {
                return BadRequest(result);
            }
        }

        [HttpPut("{userId}")]
        public async Task<ActionResult<Response<int>>> Update(int userId, [FromBody] UsersDTO request)
        {
            var result = await _service.Update(request, userId);
            if (!result.Succeeded)
            {
                return BadRequest(result);
            }
            return Ok(result);
        }

        [HttpDelete("{userId}")]
        public async Task<ActionResult<Response<int>>> Delete(int userId)
        {
            var result = await _service.Delete(userId);
            if (!result.Succeeded)
            {
                return BadRequest(result);
            }

            return Ok(result);
        }
    }
}
