﻿using ApplicationCore.Interfaces;
using ApplicationCore.Wrappers;
using Microsoft.AspNetCore.Mvc;
using ApplicationCore.DTOs.Logs;
using ApplicationCore.DTOs.Tasks;

namespace Host.Controllers
{
    [Route("api/dashboard")]
    [ApiController]
    public class DashboardController : ControllerBase
    {
        private readonly IDashboardService _service;
        public DashboardController(IDashboardService service)
        {
            _service = service;
        }

        //[HttpGet()]
        //[Authorize]
        //public async Task<IActionResult> GastoPendienteArea()
        //{
        //    var origin = Request.Headers["origin"];
        //    return Ok("test");
        //}
        [Route("getData")]
        [HttpGet]
        public async Task<IActionResult> GetUsuarios()
        {
            var result = await _service.GetData();
            return Ok(result);
        }

        [HttpPost("create")]
        public async Task<ActionResult<Response<int>>> CreateUsuario([FromBody] TasksDTO request)
        {
            var result = await _service.Create(request);
            return Ok(result);
        }

        [HttpPut("{userId}")]
        public async Task<ActionResult<Response<int>>> Update(int userId, [FromBody] TasksDTO request)
        {
            var result = await _service.Update(request, userId);
            if(!result.Succeeded) 
            {
                return BadRequest(result);
            }
            return Ok(result);
        }

        [HttpDelete("{userId}")]

        public async Task<ActionResult<Response<int>>> Delete(int userId)
        {
            var result = await _service.Delete(userId);
            if (result.Result == 0)
            {
                return BadRequest(result);
            }

            return Ok(result);
        }

        [HttpGet("getIp")]
        public async Task<ActionResult<Response<string>>> GetIpAdress()
        {
            var result = await _service.GetIp();
            return Ok(result);
        }


        [HttpPost("create-logs")]
        public async Task<ActionResult<Response<int>>> CreateLog([FromBody] LogsDTO request)
        {
            var result = await _service.CreateLog(request);
            return Ok(result);
        }

    }
}
