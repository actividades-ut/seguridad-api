﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationCore.DTOs.Tasks
{
    public class TasksDTO
    {
        public string Title { get; set; }
    }
}
