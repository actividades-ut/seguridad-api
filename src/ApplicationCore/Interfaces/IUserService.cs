﻿using ApplicationCore.DTOs.Users;
using ApplicationCore.Wrappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationCore.Interfaces
{
    public interface IUserService
    {
        Task<Response<object>> GetAllData();
        Task<Response<object>> GetPagedData(int pageNumber, int pageSize);
        Task<Response<int>> Create(UsersDTO request);

        Task<Response<int>> Update(UsersDTO request, int userId);

        Task<Response<int>> Delete(int userId);
    }
}
