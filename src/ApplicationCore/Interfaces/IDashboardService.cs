﻿using ApplicationCore.DTOs;
using ApplicationCore.DTOs.Logs;
using ApplicationCore.DTOs.Tasks;
using ApplicationCore.Wrappers;

namespace ApplicationCore.Interfaces
{
    public interface IDashboardService
    {
        Task<Response<object>> GetData();

        Task<Response<int>> Create(TasksDTO request);

        Task<Response<int>> Update(TasksDTO request, int userId);

        Task<Response<int>> Delete(int userId);

        Task<Response<string>> GetIp();

        Task<Response<int>> CreateLog(LogsDTO request);


    }
}
