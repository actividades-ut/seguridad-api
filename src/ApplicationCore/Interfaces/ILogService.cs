﻿using ApplicationCore.DTOs.Logs;
using ApplicationCore.Wrappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationCore.Interfaces
{
    public interface ILogService
    {
        Response<string> GetIp();

        Task<Response<int>> CreateLog(LogsDTO request);
    }
}
