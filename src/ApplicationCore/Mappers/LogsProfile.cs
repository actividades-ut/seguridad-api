﻿using ApplicationCore.DTOs.Logs;
using AutoMapper;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationCore.Mappers
{
    public class LogsProfile: Profile
    {
        public LogsProfile()
        {
            CreateMap<LogsDTO, logs>()
                .ForMember(x => x.id, y => y.Ignore())
                .ForMember(x => x.ip, y => y.Ignore());
        }
    }
}
