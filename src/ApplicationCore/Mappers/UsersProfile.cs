﻿using ApplicationCore.DTOs.Users;
using AutoMapper;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationCore.Mappers
{
    public class UsersProfile: Profile
    {
        public UsersProfile()
        {
            CreateMap<UsersDTO, Users>()
                .ForMember(x => x.id, y => y.Ignore());
        }
    }
}
