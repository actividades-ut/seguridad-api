﻿using ApplicationCore.DTOs.Tasks;
using AutoMapper;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationCore.Mappers
{
    public class TasksProfile: Profile
    {
        public TasksProfile() {        
        CreateMap<TasksDTO, tasks>()
            .ForMember(x => x.PkTask, y => y.Ignore());
        }
    }
}
