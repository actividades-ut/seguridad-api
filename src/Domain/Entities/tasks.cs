﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    [Table("Task")]
    public class tasks
    {
        [Key]
        //public int PkUser { get; set; }
        //public string FirstName { get; set; }
        //public string LastName { get; set; }
        //public int Age { get; set; }
        //public string Email { get; set; }
        //public DateTime FechaRegistro { get; set; }
        //public bool Estatus { get; set; }
        public int PkTask { get; set; }
        public string Title { get; set; }

    }
}
