﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    [Table("users")]
    public class Users
    {
        [Key]
        public int id { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set;}
        public string email { get; set; }
        public string gender { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public string city_of_birth { get; set; }
        public DateTime birthday  { get; set; }
        public string phone { get; set; }
        public string job_title { get; set; }

    }
}
