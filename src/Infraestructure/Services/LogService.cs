﻿using ApplicationCore.DTOs.Logs;
using ApplicationCore.Wrappers;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using ApplicationCore.Interfaces;
using AutoMapper;
using Infraestructure.Persistence;

namespace Infraestructure.Services
{
    public class LogService: ILogService
    {
        private readonly ApplicationDbContext _dbContext;
        private readonly IMapper _mapper;
        
        public LogService(ApplicationDbContext dbContext, IMapper mapper)
        {
            _dbContext = dbContext;
            _mapper = mapper;
        }

        public async Task<Response<int>> CreateLog(LogsDTO request)
        {
            try
            {
                var ip = GetIp();
                var newLog = _mapper.Map<Domain.Entities.logs>(request);
                newLog.ip = ip.Message;
                await _dbContext.Logs.AddAsync(newLog);
                await _dbContext.SaveChangesAsync();
                return new Response<int>(newLog.id, "Log creado exitosamente");
            }
            catch (Exception ex)
            {
                return new Response<int>(0, ex.Message);

            }
        }

        public Response<string> GetIp()
        {
            IPHostEntry host = Dns.GetHostEntry(Dns.GetHostName());
            IPAddress iPAddress = host.AddressList.FirstOrDefault(ip => ip.AddressFamily == AddressFamily.InterNetwork);

            var ip = iPAddress.ToString() ?? "No se pudo determinar la dirección Ip";
            return new Response<string>(ip);
        }
    }
}
