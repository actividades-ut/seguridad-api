﻿using ApplicationCore.DTOs;
using ApplicationCore.DTOs.Tasks;
using ApplicationCore.Interfaces;
using ApplicationCore.Wrappers;
using AutoMapper;
using Dapper;
using Infraestructure.Persistence;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.EntityFrameworkCore;
using System.Net.Sockets;
using System.Net;
using ApplicationCore.DTOs.Logs;
using System;
using System.Text.Json;

namespace Infraestructure.Services
{
    public class DashboardService : IDashboardService
    {
        private readonly ApplicationDbContext _dbContext;
        private readonly ICurrentUserService _currentUserService;
        private readonly IMapper _mapper;

        public DashboardService(ApplicationDbContext dbContext, ICurrentUserService currentUserService, IMapper mapper)
        {
            _dbContext = dbContext;
            _currentUserService = currentUserService;
            _mapper = mapper;
        }

        public async Task<Response<object>> GetData()
        {
            object list = new object();
            list = await _dbContext.Tasks.ToListAsync();

            return new Response<object>(list);
        }

        public async Task<Response<int>>Create(TasksDTO request)
        {
            var newUser = _mapper.Map<Domain.Entities.tasks>(request);
            string dataJson = JsonSerializer.Serialize(newUser);
            try
            {
                await _dbContext.Tasks.AddAsync(newUser);
                await _dbContext.SaveChangesAsync();

                LogsDTO log = new LogsDTO();
                log.response = "Success";
                log.data = dataJson;
                log.function = "Create User";
                await CreateLog(log);

                return new Response<int>(1);

            } catch (Exception ex)
            {
                LogsDTO log = new LogsDTO();
                log.response = ex.Message;
                log.data = dataJson;
                log.function = "Create User";
                await CreateLog(log);
                return new Response<int>(0);
            }
        }

        public async Task<Response<int>> Update(TasksDTO request, int userId)
        {
            try
            {

                var existingUser = await _dbContext.Tasks.FindAsync(userId) 
                    ?? throw new Exception("Usuario no encontrado");

                _mapper.Map(request, existingUser);

                await _dbContext.SaveChangesAsync();

                return new Response<int>(1);
            } catch (Exception ex)
            {
                LogsDTO log = new LogsDTO();
                log.response = ex.Message;
                log.data = "Id: " + userId.ToString();
                log.function = "Edit user";
                await CreateLog(log);
                return new Response<int>(ex.Message);
            }
        }

        public async Task<Response<int>> Delete(int userId)
        {

            try
            {
                var existingUser = await _dbContext.Tasks.FindAsync(userId)
                    ?? throw new Exception("Usuario no encontrado");

                _dbContext.Remove(existingUser);
                await _dbContext.SaveChangesAsync();

                return new Response<int>(1, "Tarea borrada de manera exitosa");

            } catch (Exception ex)
            {
                LogsDTO log = new LogsDTO();
                log.response = ex.Message;
                log.data = "Id: " + userId.ToString();
                log.function = "Delete user";
                await CreateLog(log);
                return new Response<int>(ex.Message);
            }

        }

        public async Task<Response<int>> CreateLog(LogsDTO request)
        {
            try
            {
            var ip = await GetIp();
            var newLog = _mapper.Map<Domain.Entities.logs>(request);
            newLog.ip = ip.Message;
            await _dbContext.Logs.AddAsync(newLog);
            await _dbContext.SaveChangesAsync();
            return new Response<int>(newLog.id, "Log creado exitosamente");

            } catch (Exception ex)
            {
                Console.WriteLine(ex);
                return new Response<int>(0, "Error");

            }
        }

        public async Task<Response<string>> GetIp()
        {
            IPHostEntry host = Dns.GetHostEntry(Dns.GetHostName());
            IPAddress iPAddress = host.AddressList.FirstOrDefault(ip => ip.AddressFamily == AddressFamily.InterNetwork);

            var ip = iPAddress.ToString() ?? "No se pudo determinar la dirección Ip";
            return new Response<string>(ip);
        }
    }
}
