﻿using ApplicationCore.DTOs.Logs;
using ApplicationCore.DTOs.Users;
using ApplicationCore.Interfaces;
using ApplicationCore.Wrappers;
using AutoMapper;
using Infraestructure.Persistence;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace Infraestructure.Services
{
    public class UserService: IUserService
    {
        private readonly ApplicationDbContext _dbContext;
        private readonly IMapper _mapper;
        private readonly ILogService _logService;

        public UserService(ApplicationDbContext dbContext, IMapper mapper, ILogService logService)
        {
            _dbContext = dbContext;
            _mapper = mapper;
            _logService = logService;
        }

        public async Task<Response<object>> GetAllData()
        {
            try
            {
                object list = new object();
                list = await _dbContext.Users.ToListAsync();
                return new Response<object>(list);
            } catch (Exception ex)
            {
                return new Response<object>(ex.Message);
            }

        }

        public async Task<Response<object>> GetPagedData(int pageNumber, int pageSize)
        {
            try
            {
                int skip = (pageNumber - 1) * pageSize;

                var pagedData = await _dbContext.Users
                                          .OrderBy(x => x.id)
                                          .Skip(skip)
                                          .Take(pageSize)
                                          .ToListAsync();

                return new Response<object>(pagedData);
            } catch (Exception ex)
            {
                return new Response<object>(ex.Message);
            }
        }

        public async Task<Response<int>> Create(UsersDTO request)
        {
            var newUser = _mapper.Map<Domain.Entities.Users>(request);
            string dataJson = JsonSerializer.Serialize(newUser);
            try
            {
                await _dbContext.Users.AddAsync(newUser);
                await _dbContext.SaveChangesAsync();

                LogsDTO log = new LogsDTO();
                log.response = "Success";
                log.data = dataJson;
                log.function = "Create User";
                await _logService.CreateLog(log);

                return new Response<int>(1, "Usuario created success");

            }
            catch (Exception ex)
            {
                LogsDTO log = new LogsDTO();
                log.response = ex.Message;
                log.data = dataJson;
                log.function = "Create User";
                await _logService.CreateLog(log);
                return new Response<int>(ex.Message);
            }
        }

        public async Task<Response<int>> Update(UsersDTO request, int userId)
        {
            try
            {

                var existingUser = await _dbContext.Users.FindAsync(userId)
                    ?? throw new Exception("Usuario no encontrado");

                _mapper.Map(request, existingUser);

                await _dbContext.SaveChangesAsync();

                return new Response<int>(userId, "Usuario actualizado de manera exitosa");
            }
            catch (Exception ex)
            {
                var log = new LogsDTO
                {
                    response = ex.Message,
                    data = "Id: " + userId.ToString(),
                    function = "Edit user"
                };
                await _logService.CreateLog(log);
                return new Response<int>(ex.Message);
            }
        }

        public async Task<Response<int>> Delete(int userId)
        {

            try
            {
                var existingUser = await _dbContext.Users.FindAsync(userId)
                    ?? throw new Exception("Usuario no encontrado");

                _dbContext.Remove(existingUser);
                await _dbContext.SaveChangesAsync();

                return new Response<int>(userId, "Tarea borrada de manera exitosa");

            }
            catch (Exception ex)
            {
                LogsDTO log = new LogsDTO();
                log.response = ex.Message;
                log.data = "Id: " + userId.ToString();
                log.function = "Delete user";
                await _logService.CreateLog(log);
                return new Response<int>(ex.Message);
            }

        }
    }
}
